FROM php:7.3-fpm-alpine
#FROM php:7.3.11-fpm-alpine3.10

WORKDIR /var/www/localhost/htdocs
ADD . /var/www/localhost/htdocs

# phpの設定を反映
COPY ./docker/php/*.ini /usr/local/etc/php/conf.d/
# apacheの設定を反映
COPY ./docker/apache/conf.d/*.conf /etc/apache2/conf.d/
COPY ./docker/apache/httpd.conf /etc/apache2/

# extension等の追加
RUN apk update && apk add --no-cache \
    autoconf \
    apache2 \
    apache2-proxy \
    freetype-dev \
    git \
    gcc \
    g++ \
    vim \
    unzip \
    tzdata \
    libmcrypt-dev \
    libltdl \
    curl-dev \
    libxml2-dev \
    postgresql-dev \
    libpng-dev \
    libjpeg-turbo-dev \
    zip \
    libzip-dev \
    unzip \
    gmp-dev \
    enchant-dev \
    icu-dev \
    openldap-dev \
    make \
    supervisor \
  && docker-php-ext-install -j$(nproc) dba enchant gd gmp intl ldap mysqli opcache xmlrpc pdo_mysql iconv \
  && pecl install redis xdebug apcu igbinary mcrypt msgpack oauth uuid zip \
  && docker-php-ext-enable redis xdebug apcu igbinary mcrypt msgpack oauth uuid zip \
  && cp /usr/share/zoneinfo/Asia/Tokyo /etc/localtime \
  && apk del tzdata \
  && rm -rf /var/cache/apk/*

# composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
  && composer config -g repos.packagist composer https://packagist.jp \
  && composer global require hirak/prestissimo
RUN composer install

RUN cp .env.example .env \
  && php artisan key:generate

EXPOSE 80

# apacheとphp-fpmの実行
COPY ./docker/supervisor/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
ENTRYPOINT ["/var/www/localhost/htdocs/docker-entrypoint.sh"]
